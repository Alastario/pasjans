#include "talia.h"
#include <stdlib.h>


class Wyglad : public Talia
{
	public:

		Wyglad();

		void UstawOkno(int Width, int Height); //Ustawienie wielko�ci okna (funkcja z neta)
		void WyswietlaniePlanszy(); //Plansza z polem do gry
		void EkranStartowy(); //Ekran startowy Nowa gra/Ranking/Koniec
		void UsuwanieKarty(int x, int y); // Usuwa pole ko�o od lewego g�rnego skraja prostokata (karty)
		void EkranStartowyWyboru(int pozycja); //Zmienia pozycje wskaznika
		void WypelnianieKarty(int x, int y, int wartosc, string znak, string kolor);
		void WydajZPozosta�ych(int wartosc, string znak, string kolor);
		void ZaznaczKarte(int x, int y);
		void OdznaczKarte(int x, int y);
		void Komunikat();
		void UsunKomunikat();

};