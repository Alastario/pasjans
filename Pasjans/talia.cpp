#include "talia.h"

void Talia::SzukajKarty(Karta* &Head, int id)
{
	Karta * Current = Head;
	while (Current->id_karty != id)
	{
		Current = Current->Next;
	}
	Talia::Current = Current;
}

Talia::Talia()
{	
	Head = nullptr;
	Tail = nullptr;
	Current = nullptr;

	//uzupe�nianie talii
	int id = 52;
	Karta talia;
	//serca=kier
	//poduszki=karo
	//czarne serce=pik
	//koniczyna=trefl
	for (int i = 0; i < 4; i++)
	{
		for (int j = 13; j >0; j--)
		{
			if (i == 0)
			{
				talia.WstawNaPoczatek(Head, Tail, id,j,"czerwony","kier");
			}
			else if (i == 1)
			{
				talia.WstawNaPoczatek(Head, Tail, id, j, "czerwony", "karo");
			}
			else if (i == 2)
			{
				talia.WstawNaPoczatek(Head, Tail, id, j, "czarny", "pik");
			}
			else if (i == 3)
			{
				talia.WstawNaPoczatek(Head, Tail, id, j, "czarny", "trefl");
			}
			id--;
		}
	}
	//tasowanie talii
	talia.Tasuj_Talie(Head,Tail);
	
	//rozdannie kart na plansze
	Current = Head;
	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			wydane_karty[i][j] = Current->id_karty;
			Current = Current->Next;
		}
	}
	//karty kt�re pozosta�y w talii
	for (int  i = 0; i < 12; i++)
	{
		pozosta�e_karty[i] = Current->id_karty;
		Current = Current->Next;
	}
}

Talia::~Talia()
{
}
